/* 
Nom de l'script = RSS.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 06/09/2020

Aquest codi permet accedir als fitxers RSS de noticies de qualsevol portal i recopilar les noticies per visualitzar-les a la nostra manera dins de Telegram . 

Habitualment implementarem aquesta opció a una hora determinada emprant un activador 

El codi contempla dos formats : llistat de text i botons. 
Podeu configurar el tipus de visualització : text o botons i també el nombre màxim de noticies a visualitzar i la longitud del títol 

*/


// Caldrà afegir la comanda "/RSS"  dins del switch per si volem deixar obert que els usuaris accedeixin a visualitzar les noticies en qualsevol moment 

switch(comanda) 
{
  
...

case '/RSS': 
              var enviat = RSS(id_usuari,text,idioma); 
         break; 

... 
}


function RSS(id,tipus,idioma)
{
  
var tp = tipus.split(" "); \\ Separem el tipus dins de la comanda 
var tipus = tp[1]; 
var tipus = tipus || "text";  \\Per defecte assignem tipus text 

 
  var url ="http://ensenyament.gencat.cat/ca/rss/rss-novetats/";  // Url del RSS de Gencat Ensenyament 
  
  var feed = UrlFetchApp.fetch(url).getContentText(); // Carreguem les dades a la variable feed 
  

  var doc = XmlService.parse(feed); // Parsejem el RSS en estructura de files i columnes 
  var root = doc.getRootElement();  // Tot RSS te una etiqueta arrel de començamen que en el RSS es justament <rss> 
  var channel = root.getChild('channel'); //Carrega les dades de l'etiqueta <channel> que no farem servir 
  var items = channel.getChildren('item'); // Carrega totes les etiquetes <item> només farem servir les num_max últimes 

  
  var frase = '<a href="'+url +'"><b>Noticies RSS Departament Ensenyament</b></a>\n\n' ; 
  //inicialitzem la capçalera de la sortida, amb un enllaç al RSS
    
 
  var num_max = 4;  // Nombre màxim de noticies a publicar 
  
  var llista = new Array();    // Iniciem la llista per recollir la informació en el format botons 
   
 if(tipus=="text")  var mida_max = 100; 
  else var mida_max = 50; 
  

  
for( i in items) // Fem un bucle per anar llegint els items 
  {  // Anirem fent el bucle per cada noticia <item> mentre no estiguem al final de totes o arribi al num_max 

  
  var item = items[i]; // Va carregant cada noticia de la llista items i 
  var titol = item.getChildText('title'); // Recuperem el titol de la noticia que toca 
  var url = item.getChildText('link'); // Recuperem l'enllaç a la noticia 
  var author = item.getChildText('author'); // Recuperem l'autor 
  var dia = item.getChildText('pubDate'); // Recuperem la Data 
  var descri = item.getChildText('description'); // Recuperem la descripció 
  
  var fecha0 = new Date(dia); // Convertim la data del RSS a data del GAS 
  var fecha  = Utilities.formatDate(fecha0,"GMT+2", "dd-MM"); // Donem format a la data 
  var mida_max = 100; // Limitem els titols de les noticies 
   
  var titol0 = titol.slice(0,mida_max); // Tallem el nombre de caracters del títol 
  
  // Si passem del maxim afegim el "..."
  if((titol0.length + 7 ) > mida_max )    titol0 = titol0 + "... [" + fecha + "]" ; 
     else  titol0 = titol0 + " [" + fecha + "]" ; 
           
         
    var titol0 = escriu_frase(titol0,idioma) ; // traduim el text de la noticia 

  // Construim la linia del titol en el format text 
  var frase = frase + (+i+1) + '.-  <a href="' + url + '"><b>' + titol0 + "</b></a> \n";  
  
  llista.push([{'text': (+i+1)  + "-" + titol0, 'url': url  }]); // Afegim la noticia a la llista , inclim el títol i la url    
    
    if(i==num_max) break; // Contolem no sobrepassar el màxim de noticies a publicar 
    }

// Segons el tipus fem una o altre sortida 
if(tipus == "text")  sendText(id,frase); 
else 
{
  var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  };
    sendText(id, '<a href="http://ensenyament.gencat.cat/ca/rss/rss-novetats/"><b>Noticies RSS Departament Ensenyament</b></a>', tecles  );  


}
 
  return true; 
  
  }
  
  