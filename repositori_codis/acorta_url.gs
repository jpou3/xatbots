/* 
Nom de l'script = acorta_url.gs

Propuesta inicial de @pfelipm adaptada para Telegram por @FerranMas
Autor = @FerranMas  / profefp.tk@gmail.com 


Url de Tinyurl = https://tinyurl.com/api-create.php?url=url_a_acortar 
Petición GET y devuelve respuesta en formato texto 

*/


function acortar(url)
{
   var TINYURL = "https://tinyurl.com/api-create.php?url="+url ; 

   var urlCorto = UrlFetchApp.fetch(TINYURL).getContentText(); 
 

return urlCorto; 
}
