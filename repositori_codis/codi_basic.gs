/*
Aquest codi es pot considerar una plantilla basica per crear el primer xatbot 
Els passos a seguir serien : 

1.- Registrar el xatbot a BotFather de telegram . Podeu aprofitar i configurar amb les 3 comandes que teniu programades info,random i me . 
2.- Crear un document Google Apps Script o un Full de Calcul al Drive 
3.- Obrir l'espai de codi i enganxar aquest codi que teniu aquí 
4.- Personalitzeu variables inicials token i ssId si feu servir full de càlcul 
Posteriorment una vegada 
5.- Publiqueu aquest codi i obtindreu la url de la vostra applicació que podeu omplir a la variable webAppUrl 
6.- Registreu la vostra url a telegram fent servir la sintaxi : 
https://api.telegram.org/botAPI_KEY/setWebhook?url=url_WebAppUrl  
7.- Finalment si tot ha anat bé ja tindreu operatiu el vostre xatbot 

*/ 


var token = "";  // Api token del vostre bot que obtindreu al BotFather 
var telegramUrl = "https://api.telegram.org/bot" + token;
var webAppUrl = "";  // Url de l'escript de Google 
var ssId = "";  // Id del full de c�lcul que esteui treballant 


//https://api.telegram.org/botAPI_KEY/setWebhook?url=url_WebAppUrl 


// Funcions per enviar a Telegram 

function sendPhoto(id,foto,caption)
{
var url = telegramUrl + "/sendPhoto?chat_id=" + id + "&photo=" + foto+"&caption=" + caption ;
 
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}

function sendText(chatId,text,keyBoard){
  keyBoard = keyBoard || 0;

 
  if(keyBoard.inline_keyboard || keyBoard.keyboard){
     var data = {
      method: "post",
      payload: {
         method: "sendMessage",
         chat_id: String(chatId),
         text: text,
         parse_mode: "HTML",
         reply_markup: JSON.stringify(keyBoard)
       }
     }
    }else{
      var data = {
        method: "post",
        payload: {
          method: "sendMessage",
          chat_id: String(chatId),
          text: text,
          parse_mode: "HTML"
        }
      }
    }

   UrlFetchApp.fetch( telegramUrl + '/', data);

 }

/* Codis HTML permessos per Telegram 

<b> negreta </b>, <strong> negreta </strong>
<i> cursiva </i>, <em> cursiva </em>
<u> subratllar </u>, <ins> subratllar </ins>
<s> strikethrough </s>, <strike> strikethrough </strike>, <del> strikethrough </del>
<b> negreta <i> cursiva negreta <s> cursiva en negreta en cursiva </s> <u> subratlla en negreta en cursiva </u> </i> negreta </b>
<a href="http://www.example.com/"> URL en l�nia </a>
<a href="tg://user?id=123456789"> esment en l�nia d'un usuari </a>
<code> codi d'amplada fixa en línia </code>
<pre> Bloc de codi de l'amplada fixa preformatat </pre>
<pre> <code class = "language-python"> bloc de codi preformatat d'amplada fixa escrit en el llenguatge de programació de Python </code> </pre>

*/


function sendDocument(id,fitxer)
{
var url = telegramUrl + "/sendDocument?chat_id=" + id + "&document=" + fitxer;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}

function sendVideo(id,fitxer)
{
var url = telegramUrl + "/sendVideo?chat_id=" + id + "&video=" + fitxer;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}

function sendLocation(id,lat,long)
{
var url = telegramUrl + "/sendlocation?chat_id=" + id + "&latitude=" + lat + "&longitude=" + long ;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}


function sendDocument2(chatId,id,caption){

  var fileId = id ;
  var img = DriveApp.getFileById(id);  
  var blob2 = img.getBlob().getAs("text/plain");
 

  var payload = {
          method: "sendDocument",
          chat_id: String(chatId),
          document: blob2,
          caption : caption,
          parse_mode: "HTML"
          //disable_web_page_preview: true,
  };
 
  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };
     
   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }



function deleteMessage(id,id_missatge)
{
var url = telegramUrl + "/deleteMessage?chat_id=" + id + "&message_id=" + id_missatge;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}


function getFile(file_id) {
  var url = telegramUrl + "/getFile?file_id=A...EC";
  var response = UrlFetchApp.fetch(url);
   Logger.log(response.getContentText());
}


function sendText3(id,text)
{
//var url = telegramUrl + "/sendMessage?chat_id=" + id + "&text=" +text +"&parse_mode=html" ;
 url = telegramUrl + "/sendMessage?chat_id=" + id + "&text=" +text ;
var response = UrlFetchApp.fetch(url);
Logger.log(response.getContentText());
}

function sendPhoto3(chatId,blob2,caption){

  var payload = {
          method: "sendPhoto",
          chat_id: String(chatId),
          photo: blob2,
          caption : caption,
          parse_mode: "HTML"
    
          //disable_web_page_preview: true,
  };
 
  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };
     
   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }


function downloadFile(fileURL,folder) {
  
  var fileName = "";
  var fileSize = 0;
  
  var response = UrlFetchApp.fetch(fileURL, {muteHttpExceptions: true});
  var rc = response.getResponseCode();
  
  if (rc == 200) {
    var fileBlob = response.getBlob()
    var folder = DocsList.getFolder(folder);
    if (folder != null) {
      var file = folder.createFile(fileBlob);
      fileName = file.getName();
      fileSize = file.getSize();
    }
  }
    
  sendText(id,"Gravat");
}


// Fi de funcionsde Telegram 

function doPost(e) {
  
  var data = JSON.parse(e.postData.contents); // Assigna les dades pasades per Telegram en format JSON a una variable data 

//MailApp.sendEmail('email', "titol" ,JSON.stringify(data,null,4) ) ;   
//Logger.log(data)  ; 

try{

if(data.message)  // En cas de que no fem servir callback 
{
  
var text = data.message.text;  // Recupera el text del missatge 
var id = data.message.chat.id;  // Recupera el id de la finestra d'on procedeix el missatge 
var id_usuari = data.message.from.id; // Recupera el id de l'usuari que ha escrit el missatge 
var id_missatge = data.message.message_id; // Recupera el id del missatge
var update_id =   data.update_id; // Recupera el id del missatge final 
var lang = data.message.from.language_code ;  // Recupera l'idioma que te el Telegram de l'usuari que ha enviat el missatge 
var nom = data.message.from.first_name ;  // Recupera tot el nom de l'usuari que ha enviat el missatge 
var location = data.message.location; 
}
}
catch(err){
    Logger.log(err); //
  }


try{
  
if(data.callback_query)
  {
  var id_usuari = data.callback_query.from.id; 
  var id = data.callback_query.message.chat.id;   
  var id_missatge = data.callback_query.message.message_id; // Recupera el id del missatge
  var text = data.callback_query.data;
  var usuari =  data.callback_query.from.user_name; 
  var nom =  data.callback_query.from.first_name; 
  var lang =  data.callback_query.from.language_code; 
 }  
  
catch(err){
    Logger.log(err); //
  }


var idioma = lang; 

var enviat = false; 
  
  
  
var entrada = text.split('@');  // Separa les paraules entrades en una matriu/array per tenir per una banda la comanda i d'altre els valors 
var comanda = entrada[0]; //La comanda serà la primera paraula. Es comença a comptar per zero

var comanda0 = comanda.split(' '); // Separa la comanda dels parametres que porti la comanda 
var comanda = comanda0[0];      // La comanda quedara a la esquerra serà la posició 0 de la llista 
  
  
switch(comanda) 
      {
        case '/start'  :   // Fabrica el missatge de benvinguda en l'idioma del Telegram de l'usuari 
             var resposta = 'Benvingut/Benvinguda al xatbot '; 
        break;
          
        case '/info':  // Mostra informació 
             var enviat = info(id_usuari,idioma); 
        break;  
                      
        case '/random':  // Mostra un avatar al.leatòri
             var enviat = avatar(id_usuari,idioma); 
        break;  
                              
        case  '/me' : // Mostra la propia informació a Telegram 
           var enviat = canvia_idioma(id_usuari,text,idioma) ; 
          break; 
          
        default   :  // Fa aquesta opció quan no ha trobat cap anterior 
          var resposta = "No entenc el que em demanes:" + text  ;
             break;
    }

  if(enviat != true) sendText(id,resposta);  // Envia la resposta 

}


function info(id,idioma)
{

var frase = " Aquest Xatbot   te com objectiu ajudar-te en el desenvolupament del teu projecte  " + 
             "Pots accedir a les comandes del xatbot clicant sobre la '/' i escollint l'opció desitjada \n\n "+ 
             "Les comandes que pots activar directament son:  \n\n" +  
             "/info -  Informació sobre les Jornades \n"+ 
             "/llistat - Llistat   \n "+
             "/random - Al.leatòri\n"+ 
             "/fotos - Photos \n " + 
             "/idioma - Selecciona l'idioma de comunicació del xatot \n " ;
             
  

 sendText(id,escriu_frase(frase,idioma));   
   
  return true; 
     
}


function escriu_frase(frase,idioma)
{

  if(idioma.length ==2 && idioma !="ca") var frase= LanguageApp.translate(frase, 'ca', idioma); // Si els dos idiomes coincideixen dona error, per això excluim fer traducció si l'idioma de l'usuari és el català 
 
  return frase ; 
  
}


function menu_idioma(id,idioma)
{
 
 var llista = new Array(
   [{"text": escriu_frase("Castellà",idioma), "callback_data": "/idi es"},
    {'text': escriu_frase("Català",idioma), "callback_data" : "/idi ca"},
    {"text": escriu_frase("Basc",idioma),"callback_data" : "/idi eu"}],
   [{"text": escriu_frase("Anglès",idioma),"callback_data" : "/idi en"},
    {"text": escriu_frase("Francès",idioma),"callback_data" : "/idi fr"},
    {"text": escriu_frase("Gàlleg",idioma),"callback_data" : "/idi gl"}],
   [{"text": escriu_frase("Àrab",idioma), "callback_data" : "/idi ar"},
    {"text": "Tancar","callback_data" : "/tancar"}]) ; 
  
  var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  }; 
  
  sendText(id,escriu_frase("Selecciona idioma",idioma),tecles );  

  return true; 
  
}

 
function avatar(id_usuari)
{
  //https://picsum.photos/200 alternativa a robohash 

  
var llista_paraules = new Array("computer","chair","water","glass","dog","mouse","bus" ); 
var paraula = llista_paraules[Math.floor(Math.random() * lista_palabras.length)];                           
 var url = "https://robohash.org/" + palabra ;  // Creem la url del generador d'avatars a partir d'una paraula
  
  sendPhoto(id,url,paraula);  // Enviem la imatge a Telegram 
         
           
  var llista = new Array([{"text" : "Si", "callback_data": "1" } , {"text" : "No", "callback_data": "2"}]) ; // Llista de les dues opcions de votació 
               var tecles =  { inline_keyboard : llista    , resize_keyboard: true,one_time_keyboard : true  };  // Assigna la llista de botons a la comanda keyboard 
               sendText(id,"¿T'agrada la imatge? ",tecles );  // Envia un missatge a Telegram emprant la funció sendText()  

  
  return true;  // Torna "cert" per evitar que no s'envii una segona resposta
}



function me(id_usuari,nom,idioma)
{
 
  sendText(id_usuari," El teu nom a Telegram és : " + nom + " fas servir Telegram en idioma " + lang ); 
  
  return true; 
  
} 

   
 




