/* 
Nom de l'script = codi_qr.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 01/09/2020

Aquest codi permet crear codis QR personalitzats dels usuaris que tinguem al nostre grup o canal de Telegram o a partir d'una llista en el full de càlcul del Drive . 

Una primera funció 'crear_qr()' genera el codi emprant l'API on-line de Google , recupera la imatge i l'envia a la finestra privada del Telegram . 

Aquest codi QR te com a url la del nostre bot i dos parametres, un l'acció que serà 'valida_entrada' i 'altre el id del usuari a Telegram 

Una segona funció 'valida_entrada()' rep el parametre id_usuari via doGet i cerca en el full de càlcul aquest usuari i el marca com a llegit i la data del procés 

Podem emprar aquests codis per a moltes utilitats i son fàcils de gestionar des dels dipositius mobils dels usuaris i dels dispositius dels controladors, fent servir un simple lector de codis QR o creant-ne un amb AppInventor. 

*/


// La funció sendphoto ens permet enviar una imatge a telegram a partir d'una variable binaria (blob) 

// Les variables token i telegramUrl les tindrem declarades a la capçalera com a globals 
//var token = ""; \\el token de Telegram 
//var telegramUrl = "https://api.telegram.org/bot" + token;


function sendPhoto3(chatId,blob2,caption){

  var payload = {
          method: "sendPhoto",
          chat_id: String(chatId),
          photo: blob2,
          caption : caption,
          parse_mode: "HTML"
    
          //disable_web_page_preview: true,
  };
 
  var options = {
    method: "POST",
    payload: payload,
    muteHttpExceptions : true
  };
     
   var request = UrlFetchApp.fetch( telegramUrl + '/', options);
   Logger.log(request.getContentText());
 }


/*
 El doGet el farem servir per validar el codiQR que s'ha llegit des d'un dispositiu móbil 
*/


function doGet(e)
{
  var accio = e.parameter.accio; 
  var id = e.parameter.id; 
   
var sortida = "Error" ;   
switch (accio)
      {
       ..... 

       case 'valida_entrada'  :   // Valida la lectura codi QR
        var sortida = valida_entrada(id); 
        return HtmlService.createHtmlOutput(sortida);
       break;
  
  }
  
  
}




.... 

// Caldrà afegir la comanda "/qr"  dins del switch que enviarà a la creació del codi QR 

switch(comanda) 
{
  
...

 case '/qr': 
              var enviat = crear_qr(id_usuari,idioma); 
              break; 

... 
}


/* 
En primer lloc la funció crear_qr(id_usuari,idioma) rep el id de telegram que te l'úsuari i l'ídioma del seu Telegram amb això ens crearà un codi QR emprant l'API on-line de Google Chart i l'enviarà a la finestra privada del Telegram de l'úsuari amb la finalitat de que la mostri en algun punt d'ídentificació  "   
*/

function crear_qr(id_usuari,idioma)
{
  
        var  caption = "Entrada a les Jornades"; // Missatge que acompanya al codi 

// webAppUrl està definit a la capçalera i és la url de l'app del bot  
// Quan hem de fer servir "&" com a parametre d'un paràmetre com en aquest cas hem de fer servir l'equivalent codificat que és '%26', sinó no ho interpreta correctament 
        var url_script = webAppUrl +"?accio=valida_entrada%26id=" + id_usuari ; 
        var qr = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=" + url_script; 
       
            
       var response = UrlFetchApp.fetch(qr); // Carrega la imatge corresponent a la url generada 
       var binaryData = response.getContent(); 

        
         var blob = Utilities.newBlob(binaryData, 'image/jpeg', 'QR'); // carrega la imatge com a dades binaries en una variable 
         sendPhoto3(id_usuari,blob, caption); // Enviem la imatge al Telegram però amb  format binari ( blob) 
       
        return true;                      
      
    
}


/*
Aquesta funció valida_entrada(id_usuari) es cridada des de doGet a una petició externa que fara un lector de codis QR . La dada a utilitzar serà el id de Telegram de l'úsuari, es podria emprar qualsevol altre identificador únic.  

Aquesta funció localitzarà el id_usuari en el full i omplirà una columna amb el text de validat i l'altre amb l'horari de la gravació 

Podem ampliar la gestíó de moltes maneres diferents segons les necessitats comptatge, llistats de presencia, etc 

*/



function valida_entrada(id_usuari)
{

// el ssId és el id del full de càlcul que tindrem en la capçalera   
var sh = SpreadsheetApp.openById(ssId).getSheetByName("Usuaris");  // Accedim al full de càlcul

var dades = sh.getDataRange().getValues();  // Llegim dels dades del full 

   
   for(i in dades)  // fem el procés fila a fila 
  {
        
    var row = dades[i];  // passem la fila corresponen a la nostra variable 
    var num_usuari = row[1]; // el id el tenim a la columna 1 cal adaptar a cada cas 
    
  
    
    if(num_usuari == id_usuari) // Verifiquem si és l'úsuari  
    {
      
  
   var entrada =   row[5];  // Si ho és agafem el valor de entrada a la columna 6 
   var nom = row[2];  // Agafem el nom a la columna 3 per personalitzar missatges 
     
  if(entrada.length  <3) // Verifiquem que no s'ha llegit el codi abans, i si no s'ha llegit gravarem les dades 
  {
        
    sh.getRange(+i+1,6).setValue("Entrat");  // Gravem la paraula Entrat a la columna 6 
    sh.getRange(+i+1,7).setValue(new Date());  // Gravem la data a la columna 7 
    return " Entrada correcte per " + nom  ; // Missatge de retorn de la validació correcta 
  }  
      else return " Entrada utilitzada per" + nom ;  // Missatge d'error d'utilització anterior del codi 
        
          
}
  
  
}
   return " Problema amb l'entrada "  ; // Missatge quan no troba l'úsuari 
  
}

