/* 
Nombre del script = codigo_iniciacion_1.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Fe4cha primera versión = 28/09/2020

Este codigo se puede usar para la creación del primer xatbot el "Hola Mundo" , un poco ampliado . 
Deberéis personalizar las variables iniciales : 
var token = el Topken identificador del bot en Telegram facilitado com el BotFather
var webAppurl = La url de la aplicación que nos facilita Google cuando publicanmos nuestro codigo 
var ssId = id de la hoja de calculo que estamos trabajando y que podemos obtener des de la url de la hoja

*/



// nombre del bot 
var token = "API_TOKEN_Telegranm_"; // API Token de Telegram 
var telegramUrl = "https://api.telegram.org/bot" + token;  // Url que comunica nuestro bot con Telegram 
var webAppUrl = "";
var ssId = "ID del Canal " ; 



function sendPhoto(id,foto,caption)  // Funcion que prepara para enviar una imagen a telegram 
{
var url = telegramUrl + "/sendPhoto?chat_id=" + id + "&photo=" + foto+"&caption=" + caption ;
 
  var response = UrlFetchApp.fetch(url);
  Logger.log(response.getContentText());
}


function sendText(chatId,text,keyBoard){  // Función que prepara para enviar un Texto a Telegram 
  keyBoard = keyBoard || 0;

 
  if(keyBoard.inline_keyboard || keyBoard.keyboard){
     var data = {
      method: "post",
      payload: {
         method: "sendMessage",
         chat_id: String(chatId),
         text: text,
         parse_mode: "HTML",
         reply_markup: JSON.stringify(keyBoard)
       }
     }
    }else{
      var data = {
        method: "post",
        payload: {
          method: "sendMessage",
          chat_id: String(chatId),
          text: text,
          parse_mode: "HTML"
        }
      }
    }

   UrlFetchApp.fetch( telegramUrl + '/', data);

 }

  function doPost(e) // Funciíon que recibe los datos enviados por Telegram que seran por metodo Post 
{

// Recoje los parametros enviados  
var data = JSON.parse(e.postData.contents); // Asigna los datos pasados por Telegram en formaot JSON a la variable data 

 
var texto = data.message.text;  // Recupera el texto del mensaje
var id = data.message.chat.id;  // Recupera el id de la ventana de donde procede el mensaje 
var id_usuario = data.message.from.id; // Recupera el id del usuario que ha escrito el mensaje
var id_mensaje = data.message.message_id; // Recupera el id del mensaje
var lang = data.message.from.language_code ;  // Recupera el idioma que tiene el Telegram del usuario 
var nombre = data.message.from.first_name ;  // Recupera el nombre del usuario que ha enviado el mensaje
var location = data.message.location;  // Recupera la localización en los casos que proceda 
  
  var idioma = alta_usuario(id_usuario,nombre,texto,lang) ; 

  
  var enviado = false ;  // Variable para controlar si s eha enviado respuesta 
  
  var comando = texto.split("@");  // Separa el comando del nombre del bot utilizando la @ de separador 
  
 
  
  switch (comando[0]) // Comando condicional para analizar los posibles valores del texto 
      {
        case '/menu' :  // Si hemos enviado /menu 
          var enviado  = menu(id);  // usamos la variable enviado para recojer la respuesta de la función menu 
        break; 
  
 case 'avatar' : 
 var enviado = avatar(id); 
 break; 

 case 'Me' : 
 var enviado = me(id_usuario,nombre,lang); 
 break; 

          
case 'Frase' : 
 var  enviado = frase(id,lang) ; 
break;
         
          
case 'Notas' : 
 var  enviado = notas(id_usuario) ; 
break;
         
          
          

        case '/ayuda' : 
          var  respuesta = "La opción 'ayuda' aun no se ha implementado"; 
          break;
          
        case '/calendario' : 
          var  respuesta = "La opción 'calendario' aun no se ha implementado"; 
          break;
        
        case '/calificaciones' : 
          var  respuesta = "La opción 'calificaciones' aun no se ha implementado"; 
          break;
        
        case '/materiales' : 
          var  respuesta = "La opción 'materiales' aun no se ha implementado"; 
          break;
        
        case '/mensajes' : 
          var  respuesta = "La opción 'mensajes' aun no se ha implementado"; 
          break;
        
        case '/tareas' : 
          var  respuesta = "La opción 'ayuda' aun no se ha implementado"; 
          break;
                         
          
        default   :
          var respuesta = "La opción indicada no està contemplada";  
             break;
    }  // final de switch de opciónes 


if(!enviado)   sendText(id,respuesta); //Si no hemos enviado ninguna respuesta, envia mensaje sin botones
  
} // final de doPost 



// función para mostrar un Menú en forma de botones 
function menu(id)
{
 
  var lista = new Array(["Tiquet QR","Ver Actividades"],["Notas","Enviar mensaje"],["avatar","Me"], ["Frase"]) ; // Lista de botones, cada claudators es una mism alinea 
               var teclas =  { keyboard: lista    , resize_keyboard: true,one_time_keyboard : true  };  // Asigna la lista de botones al comando keyboard 
               sendText(id,"Selecciona opción",teclas );  // Envia un mensaje a Telegram usando la función sendText()  
return true;  // devuelve cierto para que no se envie ningun otro mensaje 
}



function avatar(id)
{
  //https://picsum.photos/200
  
var lista_palabras = new Array("computer","chair","water","glass","dog","mouse","bus" ); 
var palabra = lista_palabras[Math.floor(Math.random() * lista_palabras.length)];                           
 var url = "https://robohash.org/" + palabra ;  // Creamos la url del generador de avatares a partir de una palabra
  
  sendPhoto(id,url,paraula);  // Enviamos la imagen a Telegram 
         
           
  var lista = new Array([{"text" : "Si", "callback_data": "1" } , {"text" : "No", "callback_data": "2"}]) ; // Lista de las dos opciónes de votacion 
               var teclas =  { inline_keyboard : lista    , resize_keyboard: true,one_time_keyboard : true  };  // Asigna la lista de botones al comando keyboard 
               sendText(id,"¿Te gusta esta imagen? ",teclas );  // Envia un mensaje a Telegram usando la función sendText()  

  
  return true;  // devuelve cierto para que no se envie ningun otro mensaje 
}



function me(id_usuario,nombre,lang)
{
 
  sendText(id_usuario," Tu nombre en Telegram es : " + nombre + " usas Telegram en idioma " + lang ); 
  
  return true; 
  
} 


function canal()
{
  var sh = SpreadsheetApp.openById(ssId) ;
  var sheet = sh.getSheets();
  var dades = sheet[0].getDataRange().getValues()  
  var fila = dades[0]; 
  var frase = fila[0]; 
    
  sendText(id_canal,frase); // enviamos a la ventana del canal la frase 
 
  sheet[0].deleteRow(1); // Borramos la primera frase cada vez 
    
}  



function frase(id,idioma)
{
 
  var sh = SpreadsheetApp.openById(ssId) ;
  var sheet = sh.getSheets();
  var dades = sheet[0].getDataRange().getValues();  // Comença a contar els fulls pel 0
 
  var num =  Math.floor(Math.random() * dades.length ); 
  
  
  var row = dades[num] ; 
  var frase = row[0]; 
  
   if(idioma !="es") var frase= LanguageApp.translate(frase, 'es', idioma); // Si els dos idiomes coincideixen dona error, per això excluim fer 
  
  sendText(id,frase) ; 
  
  return true; 
  
}


function alta_usuario( id,nombre,texto,lang)
{
  
  var sh = SpreadsheetApp.openById(ssId) ;
  var sheet = sh.getSheets();
  var dades = sheet[1].getDataRange().getValues();  // Comença a contar els fulls pel 0
 

  
  for(i in dades)
  {
    var row = dades[i];
    var num_usuario = row[1]; 
    
    var idioma = row[4]; 
    if(num_usuario == id) 
    {
      var posicion = +i+1; 
      //SpreadsheetApp.openById(ssId).getSheets()[1].getRange(posicion,5).getValue(lang) ; 
      //var dia = Utilities.formatDate(time, "GMT", "MM-dd-yyyy HH:mm:ss");
      SpreadsheetApp.openById(ssId).getSheets()[1].getRange(posicion,1).setValue(new Date());        
      SpreadsheetApp.openById(ssId).getSheets()[1].getRange(posicion,4).setValue(texto);        
      
      return lang; 
      
    }
    
  }
        SpreadsheetApp.openById(ssId).getSheets()[1].appendRow([new Date(),id,nombre,texto,lang]);
        return lang
  }



function notas(id)
{
  
  var sh = SpreadsheetApp.openById(ssId) ;
  var sheet = sh.getSheets();
  var dades = sheet[1].getDataRange().getValues();  // Comença a contar els fulls pel 0
 
   
  for(i in dades)
  {
    var row = dades[i];
    var num_usuario = row[1]; 
    
   if(num_usuario == id) 
    {
      var nota1 = row[6] ; 
      var nota2 = row[7]; 
      
      var frase =         "Materia 1 =" + nota1 + "\n"; 
      var frase = frase + "Materia 2 =" + nota2 ; 
      
      sendText(id,frase); 
      return true; 
      
    }
    
  }
       sendText(id," No existe este usuario "); 
        return true; 
  }











