/* 
Nom de l'script = redueix_url.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 20/08/2020

Aquest codi permet obtenir una url escurçada utilitzant una API 
En concret fa servir l'API https://cleanuri.com/docs  i genera una url escurçava amb l asintaxi : 
https://cleanuri.com/XXXXXXX 
Trobareu més escurçadors  a https://github.com/public-apis/public-apis#url-shorteners 

*/



function escursa(url)
{

    var url_encoded = encodeURIComponent(url);  // Codifiquem l'adreça per a ser enviada 

     var payload = {    //objecte per enviar parametres en format JSON via POST 
                  "url" : url    // Url que volem escurçar 
                    };
                     
                       var options = {     // Parametres de la connexió POST 
                           method: "POST",
                               payload: payload,
                                   muteHttpExceptions : true
                                     };
                                          
                                           
                                               
                                                  
                                                       var url_uri = "https://cleanuri.com/api/v1/shorten";  // url de l'API per escurçar la url 
                                                          
                                                               var request = UrlFetchApp.fetch( url_uri,options);  // Enviem les dades a l'API i recuperem la resposta 
                                                                    var data = JSON.parse(request) ;  // Com la resposta e sen format JSON la parsegem per obtenir files i columnes 
                                                                         var url_curta = data.result_url ;  // Recollim el parametre result_url que porta la url escurçada
                                                                          
                                                                               
                                                                                  
                                                                                     
                                                                                      return url_curta ;  // retornem la url escurçada 
}
}