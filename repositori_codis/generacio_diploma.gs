/* 
Nom de l'script = generacio_diploma.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 06/09/2020

Aquest codi genera un diploma personalitzat a partir d'una plantilla i ho exporta com a document en format PDF 
Cal activar la llibreria Drive API  al menu "Recursos/Serveis avançats" 

El proces funciona de la seguent manera : 
1.- Tenim un full de calcul amb usuaris identificat amb el seu id de Telegram 
2.- Tenim una plantilla de certificat que te variables personalitzades emprant claus {nom} o [cognoms]
3.- Executarem primer la funció diploma_mail que comprobara si lúsuari te dret a certificat i si es que si mostrara un missatge amb els botons Si o No per rebre el document per mail 
4.- Tant si es si como no s'executarà la funció diploma que en el parametre text tindrà el valor de si o no rebre el document per mail 
5.- La funció diploma, identifica a l'usuari novament i carrega la plantilla i crea un document DOC nou per rebre la copia transformada, la funció combinat fa la personalització d ela plantilal dins del document nou 
6.- Finalment el document DOC nou es convertit a PDF i si s'ha escollir rebre per email aquest s'envia per correu electrònic a l'email de l'usuari existent en el full de dades 
7.- Finalment per netejar elimina el PDF generat 


*/




switch(comanda) 
      {
                   
        ......  
            
       case '/certificat' : 
           var enviat = diploma_mail(id_usuari,idioma); 
        break;  
           
        case '/diploma' : 
            var enviat = diploma(id_usuari,text,idioma); 
        break;   
          
        
        .......

        }



//Valida previament si l'usuari te dret a certificat  

function diploma_mail(id_usuari,idioma)
{

 var fila = identifica(id_usuari);   // Identifiquem la fila de l'usuari i també si no està registrat o si no mereix el certificat 
    // Farem servir la fila per escollir només un participant a l'hora de fer el document 
  

  if(fila == 0 )
  {
   sendText(id_usuari,escriu_frase("No et correspon certificat",idioma)); 
   return true;          
  }
   
  
 var llista = new Array(
   [{"text": escriu_frase("Si",idioma), "callback_data": "/diploma si"},
    {'text': escriu_frase("No",idioma), "callback_data" : "/diploma no"}]) ; 
      
  var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  }; 
  
  sendText(id_usuari,escriu_frase("Vols rebre també per email el teu certificat?",idioma),tecles );  

  return true; 
   
  
}


//funció que crea el document personalitzat per l'id de l'usuari de Telegram 
// Una vegada creat el document genera un missatge per validar si es vol rebre per email 

function diploma(id_usuari,text,idioma)
{
  
  tt = text.split(' '); // Separem la comanda /diploma si per l'espai en blanc 
  var fer_mail = tt[1] ;  // La resposta quedara a la dreta vol dir index = 1 
  
  var fila = identifica(id_usuari);   // Identifiquem la fila de l'usuari i també si no està registrat o si no mereix el certificat 
  // Farem servir la fila per escollir només un participant a l'hora de fer el document 
    
  if(fila == 0 )
  
  {
   sendText(id,escriu_frase("No et correspon certificat",idioma)); 
   return true;          
  }
  
  
  
  sendText(id_usuari,escriu_frase("Generant certificat ... Un moment  ",idioma)); // Afeguim missatge perque tarda una mica a generar document 
  
  
   
     
  full_Id = "1";              // Id del full de càlcul
  plantilla_Id = "";      // Id de la plantilla del certificat     
  final_Id = "";           // Id de la copia de la plantilla
  pdfFolder_Id = "" ;    // Id de la carpeta dels fitxers PDF al Drive 
   
    
  var plantilla = DocumentApp.openById(plantilla_Id);   // Obrim la plantilla
  var final = DocumentApp.openById(final_Id);       // Obrim la copia de la plantilla
  var plantilla_paragrafs = plantilla.getBody().getParagraphs();   // Carreguem tots els paràgrafs de la Plantilla
     
  
  var sh = SpreadsheetApp.openById(full_Id).getSheetByName("Participants");  // Accedim al full de càlcul
  var dades = sh.getRange(fila,1,1,14).getValues();   // Llegim dels dades de la fila 2, columna 1, fins la ultima fila, columna 14
  
  var participant = dades[0][1]+"_" + dades[0][2]  ; // fem servir per nom del fitxer PDF 
  var email = dades[0][4];  
  
 
  
  final.getBody().clear();  // Esborrem la plantilla Final per començar de nou un certificat
 
  dades.forEach(function(r){  // funció que es va repetint per cada fila de dades
        combinat(r[1],r[2],r[3],plantilla_paragrafs,final );    // Envia les dades a la funció combinat per fer la subtitució
  })   // fi de la funcio forEach
    
  

  final.saveAndClose();  // Per poder crear el PDF millor tancar el document 
  
   // Creació del PDF 
   var docblob = DriveApp.getFileById(final_Id).getAs('application/pdf');  // Genera el contingut PDF apartir del document Final de la combinació 
    docblob.setName("certificat_" + participant +".pdf"); 
   var file = DriveApp.getFolderById(pdfFolder_Id).createFile(docblob) ;  // Crea el fitxer PDF a partir del contingut guardat abans 
  
   var id_pdf = file.getId(); 
  
  var url = "https://drive.google.com/uc?id=" + id_pdf +"&export=download"; 
  
 
sendDocument(id_usuari,url); 
    
   
  
  // Podeucrear un missatge més elaborat per enviar el correu 
  // var missatge = "Hola " + nom + " \n" +
                 "T'adjuntem el certificat de participació que ens acabes de sol.licitar \n \n \n " +
                 " Cordialment, \n \n "+ 
                 "El secretari de les Jornades ";   
  
if(fer_mail == "Si" || fer_mail == "si")
  {
                 
MailApp.sendEmail(email, "Certificat d'assitencia" ,"T'adjuntem certificat participació" ,{attachments: file.getAs(MimeType.PDF)}) ; 
    sendText(id_usuari,escriu_frase("S'ha enviat el certificat per mail ",idioma)); 

Drive.Files.remove(id_pdf); // Elimina el PDF generat cal activar la llibreria Drive API  al menu "Recursos/Serveis avançats" 
}
 
return true; 
  
  
}   // fi de la funció diploma


// Aquesta funció substitueix les dades de l'úsuari per personalitzar el document 

function combinat(nom,cognoms,nif,plantilla_paragrafs,final)    //funció que fa la substitució
{
  plantilla_paragrafs.forEach(function(p){    // per cada paragraf
    
    final.getBody().appendParagraph(p               // Es situa en el document copia de plantilla
                                    .copy()
                                    .replaceText("{nom}",nom)   // Reemplaça el nom
                                    .replaceText("{cognoms}",cognoms) //Reemplaça els cognoms
                                    .replaceText("{nif}",nif) //Reemplaça nif
                                    );
  })   // fi de la funció forEach
 
   final.getBody().appendPageBreak();  // Si volem que els certificats estiguin junts en un unic document anem afegint pagines
 
}
 


// Aquesta funció logalitza la fila de l'usuari a partir del seu id de Telegram 

function identifica(id_usuari)
 {
  
  var sh = SpreadsheetApp.openById(ssId).getSheetByName("Participants"); 
  var dades = sh.getDataRange().getValues();  
      
  for(i in dades)
  {
    var row = dades[i];
    var num_usuari = row[11]; 
     if(num_usuari == id_usuari) 
    {
     var certificat = row[17]; 
           
      if(certificat =="No")  return 0 ; 
      else return +i+1; 
    }
    
  }

  return 0 ; // Si no trova usduari retorna 0 
}

