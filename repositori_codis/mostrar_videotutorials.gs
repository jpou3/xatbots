/*
Script per gestionar la visualització del llistat de videotutorials . 
Tenim el llistat en un full de càlcul de Google que anomanarem "videos"
A la primera columna tindrem el títol del videotutorial 
A la segona columna tindrem la url del videotutorial 

Necesitarem el ID del full que serà la var ssId 

Farem servir una funció que ens llegirà el llistat i creara uns botons tipus inline_keyboard amb la propietat url de manera que en clicar ja obrira directament el video al navegador 

*/

var token = "";
var telegramUrl = "https://api.telegram.org/bot" + token;
var webAppUrl = "";
var ssId = "";


function sendText(chatId,text,keyBoard){
  keyBoard = keyBoard || 0;

 
  if(keyBoard.inline_keyboard || keyBoard.keyboard){
     var data = {
      method: "post",
      payload: {
         method: "sendMessage",
         chat_id: String(chatId),
         text: text,
         parse_mode: "HTML",
         reply_markup: JSON.stringify(keyBoard)
       }
     }
    }else{
      var data = {
        method: "post",
        payload: {
          method: "sendMessage",
          chat_id: String(chatId),
          text: text,
          parse_mode: "HTML"
        }
      }
    }

   UrlFetchApp.fetch( telegramUrl + '/', data);

 }


   
  function doPost(e)
{
var data = JSON.parse(e.postData.contents); // Assigna les dades pasades per Telegram en format JSON a una variable data 

//MailApp.sendEmail(email, "Videotutorials" ,JSON.stringify(data,null,4) ) ;  

  try{  
if(data.message)  // En cas de que no fem servir callback 
{
var text = data.message.text;  // Recupera el text del missatge 
var id = data.message.chat.id;  // Recupera el id de la finestra d'on procedeix el missatge 
var id_usuari = data.message.from.id; // Recupera el id de l'usuari que ha escrit el missatge 
var id_missatge = data.message.message_id; // Recupera el id del missatge
var update_id =   data.update_id; // Recupera el id del missatge final 
var lang = data.message.from.language_code ;  // Recupera l'idioma que te el Telegram de l'usuari que ha enviat el missatge 
var nom = data.message.from.first_name ;  // Recupera tot el nom de l'usuari que ha enviat el missatge 
var location = data.message.location; 
}
}
  catch(err){
    Logger.log(err); //
  }

  
try{  
 if(data.callback_query)
  {
  var id_usuari = data.callback_query.from.id; 
  var id = data.callback_query.message.chat.id;   
  var id_missatge = data.callback_query.message.message_id; // Recupera el id del missatge
  var text = data.callback_query.data;
  var usuari =  data.callback_query.from.user_name; 
  var nom =  data.callback_query.from.first_name; 
  var lang =  data.callback_query.from.language_code; 
  }
  }
  catch(err){
    Logger.log(err); 
  }  
  
  
  

var idioma = lang; 

var comanda0 = comanda.split(' '); 
var comanda = comanda0[0];   

  var enviat= false;   
 

  
switch (comanda)
      {
        
      
        case '/videotutorials': 
          var enviat = videotutorials(id_usuari,idioma); 
          break; 
        
        default   :
          //var resposta = escriu_frase("Hola " + nom + " No entenc que em demanes:" + resposta,idioma)  ;
          var enviat= true; 
             break;
    }

 
if (!enviat && !grup ) sendText(id_usuari,resposta);  // Si no s'ha enviat resposta procedeix a enviar-la 

  }
  




function videotutorials(id,idioma)
{
 
    
    var dades = SpreadsheetApp.openById(ssId).getSheetByName("Videos").getDataRange().getValues();  
  
  var llista = new Array(); 
  
  
   for(i in dades)
  {
    var row = dades[i];
    var titol  = row[0]; 
    var url = row[1]; 
   
        
    llista.push([{'text': titol, 'url':  url }]);
    
  }
  
          var tecles =  { inline_keyboard: llista    , resize_keyboard: true,one_time_keyboard : true  }; 
          sendText(id,escriu_frase("Llistat de videotutorials",idioma),tecles );  
return true; 
}


function escriu_frase(frase,idioma)
{
  if(idioma !="ca" && idioma.length ==2) var frase= LanguageApp.translate(frase, 'ca', idioma); // Si els dos idiomes coincideixen dona error, per això excluim fer traducció si l'idioma de l'usuari és el catalè 

return frase;   
}






