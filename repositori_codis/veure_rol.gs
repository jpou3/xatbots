/* 
Nom de l'script = veure_rol.gs
Autor = @FerranMas  / profefp.tk@gmail.com 
Data primera versió = 07/11/2020

Aquest codi permet accedir al rol de lúsuari dins del grup o canal que ha enviat el missatge 
D'aquesta manera podem filtrar accions segons el rol d elúsuari dins de la finestra 
Els rols que ens pot tornar son : 
  - member 
  - admin
  - creator

Per això farem una consulta a l'API de Telegram amb la comanda getChatMember internament emprant el protocol cUrl amb UrlFetchApp.fetch i recuperant el resultat en format JSON 

*/

// A la capçalera tindrem 

var token = ""; // El token de Telegram 
var telegramUrl = "https://api.telegram.org/bot" + token;






function rol(id,id_usuari)
{

// Construim la url per enviar la petició a l'API de Telegram 
var url = telegramUrl + "/getChatMember?chat_id=" + id + "&user_id=" + id_usuari ;

// enviem una petició a l'API de Telegram   
  var e = UrlFetchApp.fetch(url,{ muteHttpExceptions: true }); // Retorna en format JSON 
  
  var data = JSON.parse(e); // Assigna les dades pasades per Telegram en format JSON a una variable data de files i columnes 
  
  var estat = data.result.status ; // Recuperem el parametre status 
  
 
return estat ;  // Retornem l'status de l'úsuari dins de la finestra 
  
}
